import tpOO.tp03.Task;
import tpOO.tp03.TaskStatus;
import tpOO.tp03.ToDoList;
import java.time.*;
import java.util.*;

/**
 * Un évènement est un fait qui survient à un moment donné, et pour une certaine durée.
 * @author Hugo D
 */
public class Event {
    /*** Attribut label pour la classe*/
    private String label;
    /*** Attribut place pour la classe */
    private String place;
    /***Attribut start pour la classe */
    private LocalDate start;
    /***Attribut end pour la classe */
    private LocalDate end;
    /***Liste de tache de type ToDoList */
    private ToDoList tasks;

    /** 
     * Constructeur de la classe Event avec les attributs suivants :
     * @param label Attribut label de la classe
     * @param place Attribut place de la classe
     * @param start Attribut start de la classe
     * @param end Attribut end de la classe
     * @param tasks Nouvelle tache assigné à la classe
     */
    public Event(String label, String place, LocalDate start, LocalDate end, ToDoList tasks){
        this.label = label;
        this.place = place;
        this.start = start;
        this.end = end;
        this.tasks = tasks;
    }
}
