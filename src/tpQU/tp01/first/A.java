public class A{
    private final String LABEL = "42";
    private String label;
    
    //construtor
    public A(){
        this.label = this.LABEL;
    }

    public A(String label){
        this.label = label;
    }

    //methods
    public String toString(){
        return this.label;
    }

    public static void main(String[] args) {
        A a1 = new A();
        A a2 = new A("à choisir");
        System.out.println(a1.toString());
        System.out.println(a2.toString()); 
    }
}