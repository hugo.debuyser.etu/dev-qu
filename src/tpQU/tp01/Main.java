import controlepkg1.A;
import controlepkg2.B;
import controlepkg2.sspkg.C;
import controlpkg1.D;

public class Main{
    public static void main(String[] args) {
        A a = new A("Toto");
        B b = new B();
        C c = new C("Tata", 5);
        D d = new D();
        System.out.println(a.toString());
        System.out.println(c.toString());
        System.out.println(b.toString());
        System.out.println(d.getSomeText());
    }
}