public class A{
    private final String LABEL = "42";
    private String label;
    
    //construtor    
    public A(){
        this.label = this.LABEL;
    }

    public A(String label){
        this.label = label;
    }

    //methods
    public String toString(){
        return this.label;
    }
}